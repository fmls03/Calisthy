from app import db, User
from passlib.hash import sha256_crypt
db.create_all()
username = 'Admin'
email = 'Admin'
password = sha256_crypt.hash('Admin')
theme = 0
height = 0
weight = 0
newUser = User(username, email, password, theme, height, weight)
db.session.add(newUser)
db.session.commit()
db.session.refresh(newUser)
db.session.remove()

