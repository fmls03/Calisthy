FROM python:3.9
FROM python:3.10
FROM node:lts-alpine

WORKDIR /backend

RUN pip install -r requirements.txt

RUN python3.9 app.py

WORKDIR /
WORKDIR /frontend

RUN npm install -g npm
RUN npm install

RUN npm run serve

